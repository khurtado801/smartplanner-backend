var paymentApp = angular.module("paymentModule");
paymentApp.controller("paymentController", [
  "$scope",
  "$http",
  "config",
  "Flash",
  "$cookieStore",
  "Auth",
  "$location",
  "localStorageService",
  "cfpLoadingBar",
  function(
    $scope,
    $http,
    config,
    Flash,
    $cookieStore,
    Auth,
    $location,
    localStorageService,
    cfpLoadingBar
  ) {
    // Auth.getPaymentStatus();
    $scope.payment = function(form) {
      console.log("PAYMENT!");

      var user_id = Auth.getUserId();

      if ($scope.card_holder_name === undefined) {
        card_holder_name = "";
      } else {
        card_holder_name = $scope.card_holder_name;
      }

      if ($scope.card_number === undefined) {
        card_number = "";
      } else {
        card_number = $scope.card_number;
      }

      if (
        $scope.expiry_month === undefined ||
        $scope.expiry_month.selected === undefined
      ) {
        expiry_month = "";
      } else {
        expiry_month = $scope.expiry_month.selected.expiry_month;
      }

      if (
        $scope.expiry_year === undefined ||
        $scope.expiry_year.selected === undefined
      ) {
        expiry_year = "";
      } else {
        expiry_year = $scope.expiry_year.selected.expiry_year;
      }

      if ($scope.cvv === undefined) {
        cvv = "";
      } else {
        cvv = $scope.cvv;
      }

      var paymentObj = {
        user_id: user_id,
        //'plan_id': '1',
        plan_id: $scope.plan_id,
        plan_key: $scope.plan_key,
        card_holder_name: card_holder_name,
        card_number: card_number,
        expiry_month: $scope.expiry_month,
        expiry_year: $scope.expiry_year,
        cvv: $scope.cvv
      };
    };
    $scope.plan_change = function() {
      // console.log($scope.plan_list);
      angular.forEach($scope.plan_list, function(value, key) {
        console.log(value.id);
        if (value.id == $scope.selected_plan) {
          $scope.plan_key = value.plan_key;
          $scope.plan_name = value.name;
          $scope.duration = value.duration;
          $scope.price = value.price;
        }
      });
      $scope.plan_id = $scope.selected_plan;
      console.log(result);
    };
    $scope.getPlanDetails = function() {
      console.log("GET PLAN DETAILS:");
      var user_id = Auth.getUserId();

      $http
        .post("api/paypalpayment/getPlanDetails")
        .success(function(response) {
          console.log("GET PLAN DETAILS 2:");
          if (response.STATUS == 1) {
            var result = response.DATA;
            $scope.plan_list = result.list;
            $scope.selected_plan = result.id;
            $scope.plan_id = result.id;
            $scope.plan_key = result.plan_key;
            $scope.plan_name = result.name;
            //console.log(result);
            $scope.duration = result.duration;
            $scope.subscribe_date = result.subscribe_date;
            $scope.expire_date = result.expire_date;
            $scope.price = result.price;
            Flash.create("success", response.MESSAGE, result);
            //$location.url('/payment');
          } else {
            Flash.create("danger", response.MESSAGE);
            $location.path("/");
          }
        })
        .error(function(response) {
          Flash.create("danger", response.MESSAGE);
        });
    };

    $scope.getAllPlans = function(form) {
      console.log("GET ALL PLANS:");
      //$scope.plans = [];
      var serviceBase = config.url;
      //var allplans = [];
      $http
        .post(serviceBase + "paypalpayment/getAllPlans")
        .success(function(response) {
          if (response.STATUS == 1) {
            var result = response.DATA;
            $scope.plans = result;
            //$scope.plan.selected = $scope.plans[0];
          }
        });
    };

    $scope.list_payments = function() {
      console.log("LIST PAYMENTS:");
      var user_id = Auth.getUserId();
      var Obj = { user_id: user_id };
      var serviceBase = config.url;
      $http
        .post(serviceBase + "users/getMyAllPayments", Obj)
        .success(function(response) {
          if (response.STATUS == 1) {
            console.log(response.RESULT["my_payments"]);
            $scope.payments = response.RESULT["my_payments"];
            $scope.curr_status = response.RESULT["my_payments"]["status"];
          } else {
            $scope.payments = "";
          }
        })
        .error(function(response) {
          $scope.payments = "";
        });
    };

    $scope.filterValue = function($event) {
      if (isNaN(String.fromCharCode($event.keyCode))) {
        $event.preventDefault();
      }
    };

    $scope.changeSubscriptionStatus = function(val, trans_id) {
      console.log("changeSubscriptionStatus:");
      var Obj = {
        transaction_id: trans_id,
        status: val
      };
      var serviceBase = config.url;
      $http
        .post(serviceBase + "paypalpayment/changeRecurringStatus", Obj)
        .success(function(response) {
          if (response.STATUS == 1) {
            console.log("TEST");
            if (val == "Suspend" || val == "Cancel") {
              $cookieStore.put("subscribe", "0");
            } else {
              $cookieStore.put("subscribe", "1");
            }
            Flash.create("success", response.MESSAGE);
          } else {
            Flash.create("danger", response.MESSAGE);
          }
        })
        .error(function(response) {
          $scope.curr_status = "";
        });
    //   Stripe.setPublishableKey("pk_test_uViZbYAtpXAHz9Gn96famHmH00h1j8km9Z");
    };
  }
]);
