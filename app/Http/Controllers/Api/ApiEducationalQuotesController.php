<?php

namespace App\Http\Controllers\Api;

use App\Api\User;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Tymon\JWTAuth\JWTAuth;

/**
 * Added By: Karnik
 * Reason: Get All EducationalQuotes
 */
class ApiEducationalQuotesController extends Controller
{

    private $req;
    private $user;
    private $jwtAuth;

    public function __construct(Request $request, User $user, ResponseFactory $responseFactory, JWTAuth $jwtAuth)
    {
        header('Content-Type: application/json');
        $this->user = $user;
        $this->jwtAuth = $jwtAuth;
        $this->req = $request;
        $this->res = $responseFactory;
    }

    public function getAllEducationalQuotes(Request $request)
    {
        $educational_quotesDetails = DB::table('educational_quotes')
            ->select('id', 'quote_line1', 'quote_line2', 'author', 'status')
            ->orderBy(DB::raw('RAND()'))
            ->where('status', 'Active');

        if (count($educational_quotesDetails) > 0) {
            $this->resultapi('1', 'Educational Quote Found', $educational_quotesDetails);
        } else {
            $this->resultapi('0', 'No EducationalQuotes Found', $educational_quotesDetails);
        }
    }

    public function resultapi($status, $message, $result = array())
    {
        $finalArray['STATUS'] = $status;
        $finalArray['MESSAGE'] = $message;
        $finalArray['DATA'] = $result;
        echo json_encode($finalArray);
        exit;
    }

}
